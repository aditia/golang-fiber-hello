module golang-fiber-hello

go 1.15

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.0.4
	github.com/joho/godotenv v1.3.0
)
